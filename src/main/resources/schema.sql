drop  table if exists Branch;
create table Branch (
id NUMBER  NOT NULL AUTO_INCREMENT PRIMARY KEY,
name varchar2(255),
count NUMBER);

drop  table if exists student;
create table student (
id NUMBER  NOT NULL AUTO_INCREMENT PRIMARY KEY,
name varchar2(255),
address varchar(255),
branchId NUMBER references branch(id));

