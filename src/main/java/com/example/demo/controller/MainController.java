package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Student;
import com.example.demo.repository.BranchRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.ServiceClass;

@RestController
@RequestMapping("/test/boot")
public class MainController {

	@Autowired
	StudentRepository studentRepo;
	@Autowired
	BranchRepository branchRepo;
	@Autowired
	ServiceClass sClass;
	List<Object> list;


	@RequestMapping("*")
	public List<Object> welcome() {
		list= new ArrayList<Object>();
		list.add("Welcome service. Please select correct service.");
		return list;
	}

	@RequestMapping(method=RequestMethod.GET,  value= {"/students"})
	public List<Student> getAllStudents() {
		List<Student> list= studentRepo.findAll();
//		sClass.format(list);
		return list;
	}
	
	/*@RequestMapping(method=RequestMethod.GET,  value= {"/student/{name}"})
	public List<Student> getStudentByName(@PathVariable(value="name") String name) {
		return studentRepo.findByNameIgnoreCase(name);
	}*/

	@RequestMapping(method=RequestMethod.GET,  value= {"/student/updateaddress"})
	public List<Student> updateStudent(@RequestParam("name")String name ,
			@RequestParam("newAddress")String newAddress ) {
		Student student= studentRepo.findByNameIgnoreCase(name).get(0);
		student.setAddress(newAddress);
		studentRepo.save(student);
		return studentRepo.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET,  value= {"/student/branch"})
	public List<Branch> getBranch() {
		return branchRepo.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET,  value= {"/student"})
	public List<Student> getStudentWhereCondition(@RequestParam("name")String name , @RequestParam("branch")String bName) {
		return studentRepo.findByNameIgnoreCaseAndBranchName(name.toUpperCase() , bName.toUpperCase());
	}

}