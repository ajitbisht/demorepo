package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Branch;
import com.example.demo.entity.Student;

@Repository
public interface BranchRepository extends JpaRepository<Branch , Integer>{

	List<Student> findByName(String name);

	List<Student> findByNameIgnoreCase(String name);

}
