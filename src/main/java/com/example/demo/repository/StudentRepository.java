package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student , Integer>{

	List<Student> findByName(String name);

	List<Student> findByNameIgnoreCase(String name);
	
//	@Query("from student s inner join branch b on s.branch.id=b.id where upper(s.name)=upper(?1) and upper(b.name)=upper(?2)")
	@Query("select s from student s where upper(name)=:name and upper(s.branch.name)=:bName ")
	List<Student> findByNameIgnoreCaseAndBranchName(String name , String bName );

}
