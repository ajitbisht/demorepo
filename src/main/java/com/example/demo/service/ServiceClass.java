package com.example.demo.service;

import java.util.List;
import java.util.ListIterator;

import org.springframework.stereotype.Service;

import com.example.demo.entity.Student;

@Service
public class ServiceClass {

	
	public List<Student> format(List<Student> list)
	{
		ListIterator<Student> iter = list.listIterator();
		while(iter.hasNext())
		{
			iter.next().getBranch().emptyList();
		}
		return list;
	}
}
