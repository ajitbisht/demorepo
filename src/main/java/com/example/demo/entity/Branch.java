package com.example.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name="branch")
public class Branch {

	@Id
	private int id;
	
	private String name;
	private int count;
	@JsonBackReference
	@OneToMany(mappedBy="branch" , fetch=FetchType.LAZY)
	private List<Student> studentList= new ArrayList<Student>();
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public void addStudentList(Student student) {
		studentList.add(student);
	}
	
	public List<Student> getStudentList(){
		return studentList;
	}
	
	public void emptyList()
	{
		studentList= new ArrayList<Student>();
	}
}